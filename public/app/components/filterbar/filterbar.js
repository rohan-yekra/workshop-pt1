'use strict';

angular.module('app.filterbar', [])
    .controller('FilterbarController',['$window','$location',FilterbarController]);
// FilterbarController.$inject = ['$http'];
function FilterbarController($window,$location) {
    console.log("FilterbarController");
    var self = this;
    self.loc = $location;
    // console.log(angular.element('app').scope());
    // console.log(self.loc.url());
    this.selectedIndex=0;
    this.navData = {
        secondLocked : false,
        secondLabel : "Item Two"
    };

    // temp datas
    this.temp_hero_product_data = [
        { title: 'Dinosaur Island', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5957/960/DINO-PFv2_1920x1080.jpg?1431420043', buy_price: '1.00', rent_price: '0.50', small_synopsis: 'I\'m a small synopsis 1', small_synopsis_attribution: 'VICE 1', large_synopsis: 'I\'m a laaaarge synopsis. 1' },
        { title: 'Happy', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5664/960/HAPPY-PFv2-800x450.jpg?1428344549', buy_price: '2.00', rent_price: '1.00', small_synopsis: 'I\'m a small synopsis 2', small_synopsis_attribution: 'VICE 2', large_synopsis: 'I\'m a laaaarge synopsis. 2' },
        { title: 'The Motel Life', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2511/960/TML-PFv2-1920X1080-2.jpg?1421880093', buy_price: '3.00', rent_price: '1.50', small_synopsis: 'I\'m a small synopsis 3', small_synopsis_attribution: 'VICE 3', large_synopsis: 'I\'m a laaaarge synopsis. 3' },
        { title: 'Sirius', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2446/960/SIRIUS-PFv2-1920X1080-v1.jpg?1421879184', buy_price: '4.00', rent_price: '2.00', small_synopsis: 'I\'m a small synopsis 4', small_synopsis_attribution: 'VICE 4', large_synopsis: 'I\'m a laaaarge synopsis. 4' },
        { title: 'Dolphin Tale', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2783/960/DT-PFv2-1920X1080.jpg?1421880134', buy_price: '5.00', rent_price: '2.50', small_synopsis: 'I\'m a small synopsis 5', small_synopsis_attribution: 'VICE 5', large_synopsis: 'I\'m a laaaarge synopsis. 5' },
        { title: 'Dinosaur Island', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5957/960/DINO-PFv2_1920x1080.jpg?1431420043', buy_price: '1.00', rent_price: '0.50', small_synopsis: 'I\'m a small synopsis 1', small_synopsis_attribution: 'VICE 1', large_synopsis: 'I\'m a laaaarge synopsis. 1' },
        { title: 'Happy', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5664/960/HAPPY-PFv2-800x450.jpg?1428344549', buy_price: '2.00', rent_price: '1.00', small_synopsis: 'I\'m a small synopsis 2', small_synopsis_attribution: 'VICE 2', large_synopsis: 'I\'m a laaaarge synopsis. 2' },
        { title: 'The Motel Life', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2511/960/TML-PFv2-1920X1080-2.jpg?1421880093', buy_price: '3.00', rent_price: '1.50', small_synopsis: 'I\'m a small synopsis 3', small_synopsis_attribution: 'VICE 3', large_synopsis: 'I\'m a laaaarge synopsis. 3' },
        { title: 'Sirius', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2446/960/SIRIUS-PFv2-1920X1080-v1.jpg?1421879184', buy_price: '4.00', rent_price: '2.00', small_synopsis: 'I\'m a small synopsis 4', small_synopsis_attribution: 'VICE 4', large_synopsis: 'I\'m a laaaarge synopsis. 4' },
        { title: 'Dolphin Tale', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2783/960/DT-PFv2-1920X1080.jpg?1421880134', buy_price: '5.00', rent_price: '2.50', small_synopsis: 'I\'m a small synopsis 5', small_synopsis_attribution: 'VICE 5', large_synopsis: 'I\'m a laaaarge synopsis. 5' },
        { title: 'Dinosaur Island', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5957/960/DINO-PFv2_1920x1080.jpg?1431420043', buy_price: '1.00', rent_price: '0.50', small_synopsis: 'I\'m a small synopsis 1', small_synopsis_attribution: 'VICE 1', large_synopsis: 'I\'m a laaaarge synopsis. 1' },
        { title: 'Happy', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/5664/960/HAPPY-PFv2-800x450.jpg?1428344549', buy_price: '2.00', rent_price: '1.00', small_synopsis: 'I\'m a small synopsis 2', small_synopsis_attribution: 'VICE 2', large_synopsis: 'I\'m a laaaarge synopsis. 2' },
        { title: 'The Motel Life', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2511/960/TML-PFv2-1920X1080-2.jpg?1421880093', buy_price: '3.00', rent_price: '1.50', small_synopsis: 'I\'m a small synopsis 3', small_synopsis_attribution: 'VICE 3', large_synopsis: 'I\'m a laaaarge synopsis. 3' },
        { title: 'Sirius', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2446/960/SIRIUS-PFv2-1920X1080-v1.jpg?1421879184', buy_price: '4.00', rent_price: '2.00', small_synopsis: 'I\'m a small synopsis 4', small_synopsis_attribution: 'VICE 4', large_synopsis: 'I\'m a laaaarge synopsis. 4' },
        { title: 'Dolphin Tale', poster_url: 'https://ddffb4zqcuprk.cloudfront.net/assets/files/2783/960/DT-PFv2-1920X1080.jpg?1421880134', buy_price: '5.00', rent_price: '2.50', small_synopsis: 'I\'m a small synopsis 5', small_synopsis_attribution: 'VICE 5', large_synopsis: 'I\'m a laaaarge synopsis. 5' }
    ];

    angular.element($window).bind('scroll', function () {
        var $mainMenuBar = $('.filterbar'),
            $mainMenuBarAnchor = $('.filterbar_anchor'),
            $filterbarContainer = $('.filterbar_container'),
            $this_window = $(window);

        var window_top = $this_window.scrollTop();
        var div_top = $mainMenuBarAnchor.offset().top;

        if (window_top >= div_top) {

            $filterbarContainer.addClass('stick');
            $mainMenuBarAnchor.height($mainMenuBar.height());
        } else {
            $filterbarContainer.removeClass('stick');
            $mainMenuBarAnchor.height(0);
        }
    });
};


FilterbarController.prototype.tabClick = function(i) {
    console.log('Filterbar Controller');

    switch(i) {
        case 0:
            $('md-tab.overview_content').click();
            this.selectedIndex = i;
            //this.loc.path( "/overview" );
            break;
        case 1:
            $('md-tab.randomize_content').click();
            this.selectedIndex = i;
            //this.loc.path( "/randomize" );
            break;
        case 2:
            //this.loc.path( "/recommended" );
            break;
        case 3:
            //this.loc.path( "/genres" );
            break;

    }
    // $scope.$emit('loginSuccess',result);
};




FilterbarController.prototype.tabNext = function() {
    this.navData = Math.min(this.navData.selectedIndex + 1, 2) ;
};

FilterbarController.prototype.clickSortSelect = function() {
    console.log('yee');
    $('md-select').click();
};

FilterbarController.prototype.tabPrev = function() {
    this.navData = Math.max(this.navData.selectedIndex - 1, 0);
};

FilterbarController.prototype.navData = function() {
    return this.navData;
};

